const express = require('express')
const router = express.Router()
const User = require('../models/User')
const Chat = require('../models/Chat')
const Message = require('../models/Message')
const UnreadMessageNumber = require('../models/UnreadMessageNumber')
const Task = require('../models/Task')
const { default: mongoose } = require('mongoose')
const jwt = require('jsonwebtoken');
const jwtSecret = process.env.JWT_SECTER;

// check registration
const checkToken = (req, res, next)=>{
  const token = req.cookies.token
  if(!token){
    res.redirect('/login')
  }
  try{
    const decoded = jwt.verify(token, jwtSecret, {algorithm:'HS256'})
    req.userId = decoded.userId
    next()
  } catch(error){
    console.log('error')
  }
}

async function getInfoUser(req){
  console.log(req.cookie)
  const token = req.cookies.token;
  let user_n = "Unauthorized";
  let user_av = "1";
  if(token){
    console.log(token)
    const userId = jwt.verify(req.cookies.token, jwtSecret, { algorithm: 'HS256' }).userId;
    if(userId){
      console.log(userId)
      user = await User.findById(userId)
      console.log(user)
      if(user){
      console.log(user)
        user_n = user.name;
        user_av = user.avatar;
      }
    }
  }
  return {user_n, user_av}
}

function getChats(userId){
  return new Promise((resolve, reject)=>{
    Chat.find({participants: userId}).populate('participants').then(chats =>{
      const chatArr = chats.map(chat=>{
        return {chatId: chat._id,
        title: chat.title}
      })
      resolve(chatArr)
    }).catch(err=>{
      reject(err)
    })
  })
}


router.get('/registration', async(req, res)=>{
  let {user_n, user_av} = await getInfoUser(req)
  res.render('registration', {user: user_n, avatar: user_av})
})

router.get('/login', async(req, res)=>{
  let {user_n, user_av} = await getInfoUser(req)
  res.render('login', {user: user_n, avatar: user_av})
})

router.get('/', async(req, res)=>{
  let {user_n, user_av} = await getInfoUser(req)
  res.render('students', {user: user_n, avatar: user_av})
})

router.get('/chats', checkToken, async(req, res)=>{
  const userId = jwt.verify(req.cookies.token, jwtSecret, {algorithm: 'HS256'}).userId;
  let chats = await getChats(userId)
  console.log('chat list', chats)
  let {user_n, user_av} = await getInfoUser(req)
  res.render('chats', {chats: chats, user: user_n, avatar: user_av})
})

router.get('/logout',(req, res)=>{
  res.clearCookie('token');
  res.redirect("/");
});

router.get('/tasks', checkToken, async(req, res)=>{
  let {user_n, user_av} = await getInfoUser(req)
  let tasks =await getTasks()
  console.log(`tasks:${tasks}`)
  res.render('tasks', {user: user_n, avatar: user_av, tasks: tasks})
});


// -------post--------
async function getUnreadMessagesByUserId(userId) {
  try {
      const unreadMessages = await UnreadMessageNumber.find({ user: userId })
          .populate('chat', 'title') // Заповнення посилання на чат назвами
          .exec();

       
          const formattedUnreadMessages = await Promise.all(unreadMessages.map(async message => {
              const chat = await Chat.findById(message.chat);
              const latestMessage = await Message.findOne({ chat: message.chat })
                  .sort({ createdAt: -1 })
                  .exec();
              const sender = await User.findOne({_id: latestMessage.owner}).sort({ createdAt: -1 })
              .exec();
              return {
                  user: message.user,
                  chat: chat.title,
                  chatId: message.chat._id,
                  number: message.number,
                  sender: sender.name,
                  senderAvatar: sender.avatar,
                  latestMessage: latestMessage ? latestMessage.text : '' // Add the latest message text
              };
          }));
          console.log(formattedUnreadMessages)
      return formattedUnreadMessages;
  } catch (error) {
      console.error('Error getting unread messages:', error);
      throw error;
  }

}
async function incrementUnreadMessageNumber(user, chat) {
  try {
      const filter = { user, chat }; // Define the filter based on user and chat IDs
      const update = { $inc: { number: 1 } }; // Increment the 'number' field by 1
      const options = { upsert: true, new: true }; // Create a new document if it doesn't exist and return the updated document

      const result = await UnreadMessageNumber.findOneAndUpdate(filter, update, options);
      console.log('Updated document:', result);

      return result; // Return the updated document
  } catch (error) {
      console.error('Error incrementing unread message number:', error);
      throw error;
  }
}


router.post('/get_users_unread',checkToken,async (req,res)=>{

  const userId = jwt.verify(req.cookies.token, jwtSecret, { algorithm: 'HS256' }).userId;
  let unread = await  getUnreadMessagesByUserId(userId);
  unread  = JSON.stringify(unread)
  res.json(unread)

})

router.post('/add_to_unread',checkToken,(req,res)=>{
  incrementUnreadMessageNumber(req.body.userId,req.body.chatId)
 console.log(`adding to unread to open web chat ${req.body.chatId} user${req.body.userId}`)
 res.json({ success: true });

})

router.post('/login', async(req, res)=>{
  const {name, password} = req.body
  try{
    const user = await User.findOne({name: name}).exec()
    if(user.name == name && user.password == password){
      const id =new mongoose.Types.ObjectId(user._id)
      const token = jwt.sign({userId: id}, jwtSecret, {algorithm: 'HS256'})
      res.cookie('token', token, { httpOnly: true });
      res.redirect('/');
    }
  } catch(error){
    res.json({error : 'unvalid input'});
  }
})

router.post('/registration', async(req,res)=>{
  
  const {name, password,avatar} = req.body
  console.log(req.body)
  try{
   
    if(password.length>=8 && name.length>4){
      console.log(req.body.avatar)
      const user = await User.create({name, password, avatar})
      console.log(user)
      const id = new mongoose.Types.ObjectId(user._id)
      const token = jwt.sign({userId: id}, jwtSecret, {algorithm: 'HS256'})
      res.cookie('token', token, { httpOnly: true });
      let {user_n, user_av} = getInfoUser(req)
      res.redirect('/', {user: user_n, avatar: user_av});
    }else{
      res.json({error : 'unvalid input'});
    }
  } catch(error){
    res.json({error : 'unvalid input'});
  }
})

// router.post('/createChat', checkToken, async (req,res)=>{
//   console.log(req.body)
//   const userId = jwt.verify(req.cookies.token, jwtSecret, {algorithm: 'HS256'}).userId;
//   console.log(userId)
//   const user = await User.findOne({_id: userId});
//   User.findOne({name: req.body.name})
//   .then(async otherUser => {
//     if (otherUser && otherUser._id != userId) {
//       const new_chat = await Chat.create({title: req.body.chatName ? `${req.body.chatName} `:`${user.name}&${otherUser.name}`,
//       participants: [userId, otherUser._id]
//     });
//     } else {
//       console.log('Жодного користувача не знайдено');
//     }
//     console.log('CHAT CREATION MAIN JS')
//     const chats = JSON.stringify(await getChats(userId)); ///!!!
//     res.json(chats);
//   })
//   .catch(err => {
//     console.error('Помилка пошуку користувача:', err);
//   });
  
// });

async function getMessages(chatId){
  try {
    const messages = await Message.find({ chat: chatId }).populate('owner', 'name avatar').exec();
    if (!messages) {
      throw new Error('Messages not found');
    }
    console.log(messages);
    const formattedMessages = messages.map(message => ({
      id: message._id,
      text: message.text,
      owner: message.owner.name,
      owner_av: message.owner.avatar,
      createdAt: message.createdAt
    }));
    return formattedMessages;
} catch (error) {
  console.error('Error getting messages:', error);
  throw error;
}
}

function getUserFromChat(chatId) {
  return new Promise((resolve, reject) => {
    Chat.findById(chatId)
    .populate('participants', 'name')
    .exec().then(chat => {
      if (!chat) {
        reject(new Error('Chat not found'));
      }
      const usernames = chat.participants.map(participant => participant.name);
      resolve(usernames);
    }).then(res=>{
      resolve(express.response)
    })
  });
};

async function deleteUnreadMessage(userId, chatId) {
  try {
    await UnreadMessageNumber.deleteOne({ user: userId, chat: chatId });
    console.log('Unread message deleted successfully');
  } catch (error) {
    console.error('Error deleting unread message:', error);
    throw error;
  }
}


router.post('/openChat', checkToken, async(req,res)=>{
  let messages = await getMessages(req.body.chatId);
    
  const userId = jwt.verify(req.cookies.token, jwtSecret, { algorithm: 'HS256' }).userId;
  const user = await User.findById(userId);

  console.log(req.body.chatId)
  let users = await getUserFromChat(req.body.chatId);
  let response = {users: users, messages: messages, userId : userId, username: user.name};
      console.log(response)
      await deleteUnreadMessage(userId, req.body.chatId)
      response = JSON.stringify(response);
      res.json(response)
  
})

function getTasks() {
  return new Promise((resolve, reject) => {
      Task.find({})
          .populate({
              path: 'owner',
              select: 'name' 
          })
          .exec().then(tasks => {
                  const formattedTasks = tasks.map(task => ({
                      name: task.name, 
                      deadline: task.deadline,
                      owner: task.owner.name,
                      status: task.status,
                      id: task._id
                  }));
                  resolve(formattedTasks);
          }) .catch(err => {
              reject(err);
          });

           
  });
}




module.exports = router;