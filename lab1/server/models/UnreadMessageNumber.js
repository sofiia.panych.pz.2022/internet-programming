const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UnreadMessageNumberSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  chat: {
    type: Schema.Types.ObjectId,
    ref: 'Chat',
    required: true
  },
  number: {
    type: Schema.Types.Number,
    default: 1
  }
});

module.exports = mongoose.model('UnreadMessageNumber', UnreadMessageNumberSchema);