<?php

function addStudentSQL($group, $firstName, $lastName, $gender, $birthday){
  global $conn;
  $stmt = $conn->prepare("INSERT INTO students ( group_name, first_name, last_name, gender, birthday) VALUES (?, ?, ?, ?, ?)");
  $stmt->bind_param("sssss", $group, $firstName, $lastName, $gender, $birthday);
  if($stmt->execute()){
    return $stmt->insert_id;
  }
  $stmt->close();
}

function getStudentsFromSQL() {
  global $conn;
  $result = $conn->query("SELECT * FROM students");
  $students = array();
  if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
      $students[] = $row;
    }
  }
  return json_encode($students);
}

function editStudentInSQL($group, $firstName, $lastName, $gender, $birthday, $id){
  global $conn;
  $stmt = $conn->prepare("UPDATE students SET group_name=?, first_name=?, last_name=?, gender=?, birthday=? WHERE id=?");
  $stmt->bind_param("sssssi", $group, $firstName, $lastName, $gender, $birthday, $id);
  $stmt->execute();
  $stmt->close();
}

function validateName($name) {
  if (!empty($name) && preg_match('/^[A-Z]\w*/', $name)) {
    return true;
  }
  return false;
}

function validateBirthday($birthday) {
  $inputDate = strtotime($birthday);
  if ($inputDate !== false && date('Y', $inputDate) >= 1960 && date('Y', $inputDate) <= 2007) {
    return true;
  }
  return false;
}
?>