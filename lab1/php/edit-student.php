<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Content-Type');
include("functions.php");
include("db-connect.php");

$json_data = file_get_contents('php://input');
$data = json_decode($json_data, true);

if(isset($data['firstName']) && isset($data['lastName']) && isset($data['group']) && isset($data['gender']) && isset($data['birthday'])) {
  $firstName = $data['firstName'];
  $lastName = $data['lastName'];
  $birthday = $data['birthday'];
  $group = $data['group'];
  $gender = $data['gender'];
  $validBirthday = validateBirthday($birthday);
  $id = $data['id'];
  if (validateName($firstName) && validateName($lastName) && $validBirthday) {
    editStudentInSQL($group, $firstName, $lastName, $gender, $birthday, $id);
    echo json_encode(array('success' => true, 'id'=>$id));
  } else {
    echo json_encode(array('success' => false));
  }
} else {
  echo json_encode(array('success' => false,'data'=>$data));
} 
$conn->close();
?>