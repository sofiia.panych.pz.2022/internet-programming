<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Content-Type');
include("functions.php");
include("db-connect.php");

$id = $_POST['id'];
echo $id;
$stmt = $conn->prepare("DELETE FROM students WHERE id = ?");

$stmt->bind_param("i", $id);

if ($stmt->execute()) {
  echo "Object deleted successfully";
} else {
  echo "Error deleting object: " . $conn->error;
}
$stmt->close();
$conn->close();
?>