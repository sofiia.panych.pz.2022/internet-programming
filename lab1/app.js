require('dotenv').config()

const express = require('express');
const expressLayout = require('express-ejs-layouts');
const path = require('path');
const dirname = path.resolve();
const router = express.Router();
const app = express();
const http = require('http');
// const server = http.createServer(app);
const { Server } = require("socket.io");
const session = require('express-session');
const MongoStore = require('connect-mongo');
const cookieParser = require('cookie-parser');
app.use(cookieParser());
const jwt = require('jsonwebtoken');
const jwtSecret = process.env.JWT_SECTER;
const mongoose = require('mongoose')
const Chat  = require("./server/models/Chat.js")
const Message  = require("./server/models/Message.js")
const UnreadMessageNumber = require('./server/models/UnreadMessageNumber.js')
const Task = require('./server/models/Task.js')

app.use(express.urlencoded({ extended: true }));
const port = process.env.PORT || 3000;
const server = app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
  
});
 
const io = new Server(server, {
  cors: {
    origin: 'http://localhost:3000',
    methods: ['GET', 'POST'],
  },
});
const connectDB = require('./server/config/db.js');
const User = require('./server/models/User.js');
connectDB();
app.use(expressLayout)
app.set('layout','partials/main')
app.set('view engine', 'ejs')
app.set('views', path.join(__dirname, 'views'))
app.use('/assets', express.static(path.join(dirname, 'assets')))
app.use('/', require('./server/routes/main'));


app.use(session({
  secret: 'secret',
  resave: false,
  saveUninitialized: true,
  store: MongoStore.create({
      mongoUrl: process.env.MONGODB_URI
  }),
}))

async function incrementUnreadMessageNumber(user, chat) {
  try {
      const filter = { user, chat }; // Define the filter based on user and chat IDs
      const update = { $inc: { number: 1 } }; // Increment the 'number' field by 1
      const options = { upsert: true, new: true }; // Create a new document if it doesn't exist and return the updated document

      const result = await UnreadMessageNumber.findOneAndUpdate(filter, update, options);
      console.log('Updated document:', result);

      return result; // Return the updated document
  } catch (error) {
      console.error('Error incrementing unread message number:', error);
      throw error;
  }
}

async function deleteUnreadMessage(userId, chatId) {
  try {
      await UnreadMessageNumber.deleteOne({ user: userId, chat: chatId });
      console.log('Unread message deleted successfully');
  } catch (error) {
      console.error('Error deleting unread message:', error);
      throw error;
  }
}





// app.get('/chats', (req, res) => {
//   res.render('chats')
// });
// app.get('/students', (req, res) => {
//   res.render('students')
// });
function getUsersFromChat(chatId) {
    return new Promise((resolve, reject) => {
        Chat.findById(chatId)
        .populate('participants', 'name')
        .exec()
        .then(chat => {
          console.log('i am here', chat)
            if (!chat) {
              reject(new Error('Chat not found for ID: ' + chatId));
            }
            const userIds = chat.participants.map(participant => new mongoose.Types.ObjectId(participant._id));
            console.log('User names in the chat:', userIds);
            resolve(userIds);
        }).then(res=>{
            resolve(express.response)
        })
    });
};


io.on('connection', (socket) => {
  console.log('A user connected');
  console.log(socket.handshake.headers)
  if(socket.handshake.headers.cookie.split('; ')
    .find(cookie => cookie.trim().startsWith('token=')) && socket.handshake.headers.cookie.split('; ')
  .find(cookie => cookie.trim().startsWith('token='))
  .split('=')[1]){
      const token = socket.handshake.headers.cookie.split('; ')
      .find(cookie => cookie.trim().startsWith('token='))
      .split('=')[1];
      //парсимо та отримуємо айді студента 
      const decoded = jwt.verify(token,jwtSecret, { algorithm: 'HS256' });
      const userId = new mongoose.Types.ObjectId(decoded.userId);

      // Додавання поля userId до об'єкта сокету
      socket.userId = userId;
      console.log('Користувач підключений до сокета:', socket.userId);

      socket.on('message', async (messageStr)=>{
    
            console.log('Received messageStr:', messageStr);
       //JSON.parse(messageStr)
        let usersInChat = await getUsersFromChat(messageStr.data.chatId); //шось тут з айді але я дуже хочу спати
        const user = await User.findById(userId);
        console.log( messageStr.data.chatId)
        console.log(socket.userId)
        const message = await Message.create({chat:new mongoose.Types.ObjectId(messageStr.data.chatId), owner: socket.userId, text: messageStr.data.text})

        usersInChat.forEach(chatUserId => {
          const tempUserId = chatUserId.toString(); 


          
            let connectedSocket = null;
            io.sockets.sockets.forEach(socket => {
              if (socket.userId.toString() === tempUserId.toString()) {
                connectedSocket = socket;
              }

            });
            
            if (connectedSocket ) {
              console.log(`Користувач ${tempUserId} підключений до сокету.`);
          
              connectedSocket.emit('message', { chatId:   messageStr.data.chatId, text:   messageStr.data.text, owner: user.name, owner_av: user.avatar, ownerId: user._id, getterId: tempUserId});
            } else {
              console.log(`Користувач ${tempUserId} не підключений до сокету.`);
              
              incrementUnreadMessageNumber(tempUserId, messageStr.data.chatId)
            }
          
          });
      });

      socket.on('createChat', async(data)=>{
        const userId = socket.userId;
        console.log('chat user id:', userId)
        const user = await User.findOne({ _id: userId });
        User.findOne({name: data.name}).then(async otherUser => {
          if (otherUser&&otherUser._id!= userId) {
              const new_chat = await Chat.create({title: `${user.name} & ${otherUser.name}`,
              participants: [userId, otherUser._id]
          });

          io.sockets.sockets.forEach(socket => {
              if (socket.userId.toString() === userId.toString() || socket.userId.toString() === otherUser._id.toString()) {
                console.log('CREATED CHAT INFO:', new_chat)
                socket.emit('createChat', {title: new_chat.title, chatId: new_chat._id})
              }
          });

          } else {
              console.log('Жодного користувача не знайдено');
          }
               
      })
      .catch(err => {
          console.error('Помилка пошуку користувача:', err);
      });
      })

      socket.on('renameChat', async(data)=>{
        try {
          const updatedChat = await Chat.findByIdAndUpdate(data.chatId, { title: data.title }, { new: true });
          if (!updatedChat) {
              throw new Error('Chat not found');
          }
          let usersInChat = await getUsersFromChat(data.chatId); 
          usersInChat.forEach(chatUserId => {
            const tempUserId = chatUserId.toString();         
              let connectedSocket = null;
              io.sockets.sockets.forEach(socket => {
                if (socket.userId.toString() === tempUserId.toString()) {
                  connectedSocket = socket;
                }
  
              });
              
              if (connectedSocket ) {
                console.log(`Користувач ${tempUserId} підключений до сокету.`);
            
                connectedSocket.emit('renameChat', { chatId: data.chatId, title: updatedChat.title});
              } else {
                console.log(`Користувач ${tempUserId} не підключений до сокету.`);
                //incrementUnreadMessageNumber(tempUserId,message.chatId)
              }
            
            });
          console.log('Chat title updated successfully:', updatedChat);
          return updatedChat;
      } catch (error) {
          console.error('Error updating chat title:', error);
          throw error;
      }

            
      })
      socket.on('addUserToChat', async(data)=>{
        try {
          const name = data.name;
          const chatId = data.chatId
          const user = await User.findOne({ name });
        
        if (!user) {
            throw new Error('User not found');
        }

        // Знаходимо чат за id
        const chat = await Chat.findById(chatId);
        console.log(user,chat)
          if (!chat.participants.includes(user._id)) {
            chat.participants.push(user._id);
            await chat.save();
        }
          
          let usersInChat = await getUsersFromChat(data.chatId); 
          usersInChat.forEach(chatUserId => {
            const tempUserId = chatUserId.toString();         
              let connectedSocket = null;
              io.sockets.sockets.forEach(socket => {
                if (socket.userId.toString() === tempUserId.toString()) {
                  connectedSocket = socket;
                }
  
              });
              
              if (connectedSocket ) {
                console.log(`Користувач ${tempUserId} підключений до сокету.`);
            
                connectedSocket.emit('addUserToChat', { chatId: data.chatId, users: usersInChat});
              } else {
                console.log(`Користувач ${tempUserId} не підключений до сокету.`);
                //incrementUnreadMessageNumber(tempUserId,message.chatId)
              }
            
            });
      } catch (error) {
          console.error('Error updating chat title:', error);
          throw error;
      }

            
      })
      
      socket.on('move_to_next_cathegory',async (task_in) => {
        console.log(task_in)
        const task = await Task.findById(task_in.id);
        const user = await User.findById(socket.userId);
        if (task) {
            switch (task.status) {
                case 'ToDo':
                    task.status = 'WIP';
                    break;
                case 'WIP':
                    task.status = 'Done';
                    break;
                case 'Done':
                    task.status = 'ToDo';
                    break;
                default:
                    console.log('Невідомий статус');
                    break;
            } // Встановлюємо нову категорію
            await task.save(); // Зберігаємо зміни у базі даних
        } else {
            console.log('Таска не знайдена'); // Обробка ситуації, коли таска не знайдена за заданим id
        }
        io.sockets.sockets.forEach(socket => {
           // if((socket.userId.toString()!=user._id.toString())){
                socket.emit("move_to_next_cathegory",{name: task.name, deadline: task.deadline, owner: user.name, id: task._id, status: task.status})
           // }
        });
    });

      socket.on('add_task',async (task) => {
        const user = await User.findById(socket.userId);
        const newTask =  await Task.create({
            owner: socket.userId,
            name: task.name,
            deadline: task.deadline ? new Date(task.deadline) : new Date()
        });
        io.sockets.sockets.forEach(socket => {
          //  if((socket.userId.toString()!=user._id.toString())){
            console.log("emit")
            socket.emit("new_task",{name: task.name, deadline: newTask.deadline, owner: user.name, id:newTask._id})
          //  }
            
        });
    });

    socket.on('move_to_next_cathegory',async (task_in) => {
      console.log(task_in)
      const task = await Task.findById(task_in.id);
      const user = await User.findById(socket.userId);
      if (task) {
          switch (task.status) {
              case 'ToDo':
                  task.status = 'WIP';
                  break;
              case 'WIP':
                  task.status = 'Done';
                  break;
              case 'Done':
                  task.status = 'ToDo';
                  break;
              default:
                  console.log('Невідомий статус');
                  break;
          } // Встановлюємо нову категорію
          await task.save(); // Зберігаємо зміни у базі даних
      } else {
          console.log('Таска не знайдена'); // Обробка ситуації, коли таска не знайдена за заданим id
      }
      io.sockets.sockets.forEach(socket => {
         // if((socket.userId.toString()!=user._id.toString())){
              socket.emit("move_to_next_cathegory",{name: task.name, deadline: task.deadline, owner: user.name, id: task._id, status: task.status})
         // }
      });
  });
  
  } else {
    console.log('User does not have a cookie set');
  }
  

  socket.on('disconnect', () => {
      console.log('A user disconnected');
  });
});



