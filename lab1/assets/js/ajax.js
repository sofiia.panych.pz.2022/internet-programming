function sentDataToServer(student){
  console.log("json",JSON.stringify(student))
  $.ajax({
    type: "POST",
    url: 'http://localhost:8000/php/add-student.php',
    contentType: 'application/json',
    data: JSON.stringify(student),
    success: function(data) {
      console.log('Parsed data', JSON.parse(data))
      let dataParsed = JSON.parse(data);
      console.log('Send data',data)
      if(dataParsed.success){
        let newStudent = student;
        newStudent.id = dataParsed.id;
        newStudent.status = '0';
        addStudentToList(newStudent);
        updateTable()
        console.log('Data sent to the server:', JSON.stringify(newStudent));
      }else{
        alert('wasnt added');
      }
      console.log('Successful response received.');
    },
    error: function(xhr, status, error) {
      console.error('Error occurred while making the request:', error);
      console.error('Status:', status);
      console.error('XHR object:', xhr);
  }
  })
}

function editDataToServer(student){
  $.ajax({
    type: "POST",
    url: 'http://localhost:8000/php/edit-student.php',
    contentType: 'application/json',
    data: JSON.stringify(student),
    success: function(data) {
      console.log('Edit data',data)
      let dataParsed = JSON.parse(data);
      console.log('Edit data',data)
      if(dataParsed.success){
        updateTable()
      }else{
        alert('wasnt edited');
      }
      console.log('Successful response received.');
      console.log('Data sent to the server:', JSON.stringify(student));
    },
    error: function(xhr, status, error) {
      console.error('Error occurred while making the request:', error);
      console.error('Status:', status);
      console.error('XHR object:', xhr);
  }
  })
}

function getDataFromSQL(){
  return new Promise(function(resolve, reject) {
    $.ajax({
      type: "GET",
      url: 'http://localhost:8000/php/get-students.php',
      contentType: 'application/json',
      success: function(data) {
        let dataParsed = JSON.parse(data);
        console.log('Parced students:', dataParsed);
        dataParsed.forEach(student => {
          student.group = student.group_name;
          student.firstName = student.first_name;
          student.lastName = student.last_name;
          delete student.group_name;
          delete student.first_name;
          delete student.last_name;
        });
        resolve(dataParsed);
      },
      error: function(xhr, status, error) {
        console.error('Error occurred while making the request:', error);
        console.error('Status:', status);
        console.error('XHR object:', xhr);
        reject(error);
    }
    })
});
}

function deleteStudentFromSql(studentId){
  $.ajax({
    type: "POST",
    url: 'http://localhost:8000/php/delete-student.php',
    //contentType: 'application/x-www-form-urlencoded',
    data: { id: studentId },
    success: function(data) {
      console.log(data);
      console.log('Data deleted from the server:', JSON.stringify(studentId));
    },
    error: function(xhr, status, error) {
      console.error('Error occurred while making the request:', error);
      console.error('Status:', status);
      console.error('XHR object:', xhr);
  }
  })
}


function createChat(Info){
  $.ajax({
    type: "POST",
    url: '/create_chat',
    data: { name: Info.name, chatName: Info.chatName},
    success: function(data) {
     console.log(data)
     //вставляти чат
    },
    error: function(xhr, status, error) {
      console.error('Error occurred while making the request:', error);
      console.error('Status:', status);
      console.error('XHR object:', xhr);
  }
  })
}