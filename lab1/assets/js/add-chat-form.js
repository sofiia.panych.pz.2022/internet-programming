MyWind.addChatForm = function(options){
  const html_addForm = 
  `<form action="#" method="post" id="form-chat">
    <div class="form">
    <label for="name" class="label-info">Username:</label>
    <input type="text" class="username-input" id="name" name="name" placeholder="username">
    </div>
  </form>`; 
  return new Promise((resolve, reject) => {
      const modal = MyWind.modal({
        title: options.title,
        width: '400px',
        closable: false,
        content: html_addForm,
        onClose() {
          modal.destroy()
        },
        footerButtons: [
            {text: 'Cancel', type: 'cancel-type', handler() {
              modal.close()
              reject()
            }},
            {text: 'Add', type: 'create-type', handler() {
              modal.close()
              let usernameChat = document.querySelector('.username-input').value
              resolve(usernameChat)
            }}
        ]
      })
      setTimeout(() => modal.open(), 100)
  }) 
}
