window.addEventListener('load', async()=>{
  if(navigator.serviceWorker){
    try{
      const reg = await navigator.serviceWorker.register('service-worker.js')
      console.log('Service worker register success ', reg)
    } catch (e){
      console.log('Service worker register fail')
    }
    try {
      // const currentPageUrl = window.location.href;
      // if (currentPageUrl.includes('assets/students.html')) {
          const studentsFromServer = await getDataFromSQL();
          Students = studentsFromServer;
          updateTable();
      // } else if (currentPageUrl.includes('chats.html')) {
      //     console.log('Do something for chats.html');
      // }
    } catch (error) {
      console.error('Error fetching students from SQL:', error);
    }
  }
})

$(document).ready(function() {
  $('.notif-pic').addClass('ringing')

  // $('.notif-pic').click(function() {
  //   $(this).toggleClass('ringing')
  // })

  $('.notif-pic').hover(function() {
    $(this).removeClass('ringing')
  })
})

let Students = [];

// const storedStudents = localStorage.getItem('Students');
// Students = storedStudents ? JSON.parse(storedStudents) : [];

//updateTable()
console.log(Students)

function addStudentToList(student){
  //student.id = (Students.length==0)?  1:Math.max(...Students.map(student=>student.id))+1;
  //console.log("ID", student.id)
  Students.push(student)
  //localStorage.setItem('Students', JSON.stringify(Students));
}

document.querySelector(".add-btn").addEventListener(("click"), ()=>{
  MyWind.form().then((properties)=>{
    sentDataToServer({
        checked: false,
        group: properties.group,
        firstName: properties.firstName,
        lastName: properties.lastName,
        gender: properties.gender,
        birthday: properties.birthday,
        status: true,
        id: properties.id
    });
    //console.log('sended')
    //updateTable()
}).catch(()=>{});
  //localStorage.setItem('Students', JSON.stringify(Students));
  console.log(Students)
})


function openEditForm(event){
  const button = event.target.closest('.btn-in-table-edit');
  const index = Students.findIndex(student => student.id == button.dataset.id)
  MyWind.form(Students[index], {title: 'Edit student', text: 'Change'}).then((properties)=>{
    Students[index].group = properties.group
    Students[index].firstName = properties.firstName
    Students[index].lastName = properties.lastName
    Students[index].gender = properties.gender
    Students[index].birthday = properties.birthday
    //localStorage.setItem('Students', JSON.stringify(Students));
    editDataToServer(Students[index])
    //updateTable()
  }).catch(()=>{});
  
  console.log(Students)
}


function renderTableRow(student, active){
  return `<td><input id="input-${student.id}" type="checkbox" ${student.checked ? 'checked' : ''}></td><label for="input" class="label-input">L</label>
  <td>${student.group}</td>
  <td>${student.firstName} ${student.lastName}</td>
  <td>${student.gender}</td>
  <td>${student.birthday}</td>
  <td><div class="status-${active}"></div></td>
  <td>
    <button class="btn-in-table btn-in-table-edit" data-id='${student.id}'><img src="assets/icons/edit-icon.png" alt="edit" class="img-in-table"></button> 
    <button class="btn-in-table btn-in-table-delete" data-id='${student.id}'><img src="assets/icons/delete-icon.png" alt="delete" class="img-in-table img-in-table-delete"></button>
  </td>
</tr>`
}
async function updateTable(){
  let tableBody = document.querySelector(".table-body")
  tableBody.innerHTML=''
  try {
    Students = await getDataFromSQL();
  } catch (error) {
    console.error('Error fetching updated student data:', error);
    return;
  }
  Students.forEach((student)=>{
    let active = "offline"
    if(student.status===true){
      active = "online"
    }
    let studentNode = document.createElement('tr');
    studentNode.id = student.id;
    studentNode.innerHTML = renderTableRow(student, active);
    tableBody.appendChild(studentNode);
  })

  //editF()
  document.querySelectorAll(".btn-in-table-edit").forEach((button)=>{
    button.addEventListener('click', openEditForm)
  })
  document.querySelectorAll(".btn-in-table-delete").forEach((button)=>{ 
    button.addEventListener('click',()=>{
      MyWind.confirmForm({
        width: '400px',
        title: 'Are you sure?'
      }).then(async (properties) => {
        try {
          deleteStudentFromSql(Number(button.dataset.id));
          Students = await getDataFromSQL();
          updateTable();
        } catch (error) {
          console.error('Error deleting student:', error);
        }
      }).catch(()=>{});
    })
  })
}

function deleteStudentFromList(deleteID){
  let StudentTemp = []
  Students.forEach((element)=>{
    if(deleteID !== element.id){
      StudentTemp.push(element)
    }
  })
  Students = StudentTemp
  //localStorage.setItem('Students', JSON.stringify(Students));
  console.log("meow")
}