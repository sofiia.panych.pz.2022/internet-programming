function openAddChatForm(event){
  MyWind.addChatForm({title: 'Add chat', text: 'Change'}).then((usernameChat)=>{
    createChat(usernameChat)
  }).catch(()=>{});
  
  console.log(Students)
}

function createChat(usernameChat){
  // $.ajax({
  //   type: "POST",
  //   url: '/createChat',
  //   data: { name: usernameChat },
  //   success: function(response) {
  //     console.log(response);
  //     renderChats(response)
  //   },
  //   error: function(xhr, status, error) {
  //     console.error('Error occurred while making the request:', error);
  //     console.error('Status:', status);
  //     console.error('XHR object:', xhr);
  // }
  // })
  socket.emit('createChat', { name: usernameChat });
}

function renderChats(chats){
  console.log('render chats ',typeof(chats), chats)

  const chatsListElement = document.querySelector('.chat-list');
  chatsListElement.innerHTML = ``;
  const chatsArr = JSON.parse(chats);

  console.log(chatsArr);
  
  chatsArr.forEach(chat =>{
    const chatBtn = document.createElement('button')
    chatBtn.classList.add('btn', 'btn-outline-secondary', 'btn-block', 'mb-2', 'chat-list-item')
    chatBtn.id = chat.chatId
    chatBtn.addEventListener('click', (event)=>{
      openChat(event.target.id, event.target.textContent)

      document.querySelectorAll(".chat-list-item").forEach(elem=>{
        elem.classList.remove("active-chat");
      })
      event.target.classList.add("active-chat");
    })
    chatBtn.innerHTML = chat.title;
    chatsListElement.appendChild(chatBtn);
  })
}


function openChat(chatId, name){
  $.ajax({
    type: "POST",
    url: '/openChat',
    data: { chatId: chatId },
    success: function(response) {
      console.log('chat is ALMOST rendered hehehe', response)
      document.querySelector('.chat-title').textContent = name;
      renderChat(response, chatId)
      get_users_unread()
      // add changing of header here (name, users)
    },
    error: function(xhr, status, error) {
      console.error('Error occurred while making the request:', error);
      console.error('Status:', status);
      console.error('XHR object:', xhr);
  }
  })
}

function renderChat(chatInfo, chatId){
  let info = JSON.parse(chatInfo)
  console.log('renderChat', info)

  document.querySelector('.users-in-chat').innerHTML = `<h1>Users:</h1>`
  info.users.forEach(user => {
    addUser(user)
  })
  info.messages.forEach(msg => {
    addMessage(msg, info.username)
  })
  document.querySelector('.btn-submit').dataset.id = chatId;
  
  const submitButton = document.querySelector('.btn-submit');
  submitButton.disabled = false;
}
function addUser(user){
  let node = document.createElement("div");
  node.innerHTML = `<h4>${user}</h4>`;
  //document.querySelector(".users-in-chat").appendChild(node);
  document.querySelector(".users-in-chat").textContent += ` ${user} `;
}

// function addMessage(message, username){
//   const messages = document.querySelector('.chat-messages');
//     const messageBlock = document.createElement('div');
//     messageBlock.classList.add('message-block', 'pb-4');
//     messageBlock.classList.add(message.owner === username ? "chat-message-left" : "chat-message-right");

//     messageBlock.innerHTML = `
//       <img src='assets/avatars/avatar-${message.owner_av}.svg' class="rounded-circle mr-3" alt='${message.owner}' width='40' height='40'>
//       <div class="flex-shrink-1 bg-light rounded py-2 px-3 mr-3">
//         <div class="font-weight-bold mb-1">${message.owner === username ? 'You' : message.owner}</div>
//         ${message.text}
//       </div>
//       `;

//     messages.appendChild(messageBlock);
//     let chatInner = document.querySelector('.chat-messages');
//     chatInner.scrollTop = chatInner.scrollHeight;
// }


window.onload = () => {
  renderChats(chats)
  const clickedButtonId = localStorage.getItem('clickedButtonId');
    if (clickedButtonId) {
        const element = document.querySelector(`.chat-list-item[id="${clickedButtonId}"]`);
        console.log(element);
        if (element) {
            element.click(); // симуляція натискання на елемент
        } else {
            console.log('Елемент не знайдено');
        }
        localStorage.removeItem('clickedButtonId'); // Очистити збережене значення після використання
    }
};