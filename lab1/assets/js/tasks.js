
MyWind.input_task = function(options = {}){
  const html_content = `
  <form action="#" method="post" id="chat_form">
          <div class="group_"> 
              <label for="name" class="info_lable">Text:</label>
              <input class="info_input_name" id="name" name="name" placeholder="Text">
          </div>
          <div class="group_"> 
          <label for="date" class="info_lable">Date:</label>
          <input type="date" class="info_input_date" id="date" name="date">
      </div>
  </form> 
  `; 
  return new Promise((resolve, reject) => {
      const modal = MyWind.modal({
        title:`${options.title? options.title : "Enter Task"}`,
        width: '400px',
        closable: false,
        content: html_content,
        onClose() {
          modal.destroy()
        },
        footerButtons: [
          {text: 'Cancel', type: 'btn', handler() {
              modal.close()
              reject()
            }},
          {text: 'Apply', type: 'btn', handler() {
             let name = document.querySelector(".info_input_name").value;
             let date = document.querySelector(".info_input_date").value;
             
             resolve({name,date});
             modal.close()
            }}
        ]
      })
      setTimeout(() => modal.open(), 100)
  })       
}

function isValidDate(dateString) {
  // Перетворюємо стрічку дати у Date об'єкт
  let dateObject = new Date(dateString);

  // Перевіряємо, чи дата валідна (не NaN) та чи є вона більшою за поточну дату
  return !isNaN(dateObject.getTime()) && dateObject > new Date();
}

function add_task(name,deadline){
  MyWind.input_task().then((res)=>{console.log(res)
     // if(res.name.length> 4 && res.date.length>3 && isValidDate(res.date)){
          socket.emit('add_task',{name: res.name, deadline: res.date} );
     // }else{
          //flash_red.open("Wrong input")
     // }
      })
  // socket.emit('add_task',{name: name, deadline: deadline} );
}      

function move_to_next_cathegory(id){
  console.log('iddd', id)
  socket.emit('move_to_next_cathegory',{id:id} );
} 



const task_html = task => {
  return `
  <div class="card-header justify-content-between align-items-center">
    <h6 class="card-title m-0">${task.owner}</h6>
    <p class="card-text-data text-muted m-0">${task.deadline}</p>
  </div>
  <p class="card-text">${task.name}</p>
  <hr>
  `;
}

window.onload = () => {
  tasks = JSON.parse(tasks)
  socket.on('move_to_next_cathegory', (data) => {
    if(document.querySelector(".tasks")){
        console.log('Нова таска', data);
        let node = document.getElementById(data.id);
        switch (data.status) {
            case 'ToDo':
                document.querySelector(".done").removeChild(node);
                document.querySelector(".todo").appendChild(node);
                break;
            case 'WIP':
                document.querySelector(".todo").removeChild(node);
                document.querySelector(".wip").appendChild(node);
                break;
            case 'Done':
                document.querySelector(".wip").removeChild(node);
                document.querySelector(".done").appendChild(node);
                break;
            default:
                console.log('Невідомий статус');
                break;
        }
    }
});

  console.log(tasks)
  console.log(typeof(tasks))
  
document.querySelector('.add-task').addEventListener('click',()=>{
  add_task("fff", "2024-05-28")
})

  const todoTasks = tasks.filter(task => task.status === 'ToDo');
  let container = document.querySelector(".todo");
  container.innerHTML = "";
  todoTasks.forEach(element => {
      element.deadline = formatDate(element.deadline);
      let node = document.createElement("div");
      node.classList.add('taskbox');
     
      node.innerHTML = task_html(element);
      node.id = element.id;
      node.addEventListener('click',(event)=>{
        console.log("click")
          move_to_next_cathegory(node.id)
      });
      container.appendChild(node);
  });

  const wipTasks = tasks.filter(task => task.status === 'WIP');
  container = document.querySelector(".wip");
  container.innerHTML = "";
  wipTasks.forEach(element => {
      element.deadline = formatDate(element.deadline);
      let node = document.createElement("div");
      node.classList.add('taskbox');
      node.innerHTML = task_html(element);
      node.id = element.id;

      node.addEventListener('click',(event)=>{
          move_to_next_cathegory(node.id)
      });
      container.appendChild(node);
  });

  const doneTasks = tasks.filter(task => task.status === 'Done');
  container = document.querySelector(".done");
  container.innerHTML = "";
  doneTasks.forEach(element => {
      element.deadline = formatDate(element.deadline);
      let node = document.createElement("div");
      node.classList.add('taskbox');
      node.innerHTML = task_html(element);
      node.id = element.id;
      node.addEventListener('click',(event)=>{
          move_to_next_cathegory(node.id)
      });
      container.appendChild(node);
  });
};
