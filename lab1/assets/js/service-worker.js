const staticCacheName = 's-app-v4'
const dynamicCacheName = 'd-app-v4'
const assetUrls = [
  'students.html',
  'style.css',
  'script.js',
  'modal.js',
  'form.js',
  'confirm-form.js',
  'manifest.json',
  '/icons/delete-icon.png',
  '/icons/edit-icon.png',
  '/icons/kitty.jpg',
  '/icons/notification-icon.png',
  '/icons/pes-patron.jpg',
  '/icons/user-icon.png',
  'offline.html',
  'ajax.js'
]

self.addEventListener('install', event =>{
  event.waitUntil(
    caches.open(staticCacheName).then(cache => cache.addAll(assetUrls))
  )
})

self.addEventListener('activate', async event =>{
  const cacheNames = await caches.keys()
  await Promise.all(
    cacheNames.filter(name => name!==staticCacheName).filter(name => name!==dynamicCacheName).map(name=>caches.delete(name))
  )
})

self.addEventListener('fetch', event =>{
  const {request} = event
  const url = new URL(request.url)
  if(url.pathname.endsWith('.php')){
    event.respondWith(Network(request));
  }else{
    if(url.origin === location.origin){
      event.respondWith(cacheFirst(request))
    } else {
      event.respondWith(networkFirst(request))
    }
  }
})

async function Network(request){
  return await fetch(request);
}

async function cacheFirst(request){
  const cached = await caches.match(request)
  return cached ?? await fetch(request)
}

async function networkFirst(request){
  const cache = await caches.open(dynamicCacheName)
  try {
    const response = await fetch(request)
    await cache.put(request, response.clone())
    return response
  } catch (e) {
    const cached = await cache.match(request)
    return cached ?? await caches.match('/offline.html')
  }
}