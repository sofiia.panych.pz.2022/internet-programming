// function formatDate(inputDate) {
//   const date = new Date(inputDate);
//   const day = String(date.getDate()).padStart(2, '0');
//   const month = String(date.getMonth() + 1).padStart(2, '0');
//   const year = date.getFullYear();

//   return `${day}.${month}.${year}`;
// }

function getInformation(){
  const group = $('.group').val().trim()
  const firstName = $('.first-name').val().trim()
  const lastName = $('.last-name').val().trim()
  const gender = $('.select-gender').val().trim()
  const birthday = dayjs($('.birthday-input').val().trim())

  return {
    group: group,
    //name: name,
    firstName: firstName,
    lastName: lastName,
    gender: gender,
    birthday: birthday.format('YYYY-MM-DD')
  }
}

function setInformation(info){
  $('.group').val(info.group)
  $('.first-name').val(info.firstName);
  $('.last-name').val(info.lastName);
  $('.gender').val(info.gender)
  $('.birthday-input').datepicker({
    dateFormat: 'yy-mm-dd',
    onSelect: function (dateText, inst) {
    }
  });

  $('.birthday-input').val(info.birthday);
}

MyWind.form = function(options = {}, options2 = {}){
  const html_form = 
  `<form action="#" method="post" id="form-student">
    <div class="form">
    <label>Enter group</label>
    <input type="text" class="group" id="group" name="group" placeholder="Group">
    <label>Enter name</label>
    <div class="name-form">
    <input type="text" class="first-name" id="firstName" name="firstName" placeholder="First name">
    <input type="text" class="last-name" name="lastName" placeholder="Last name"></div>
    <div class="gender-form"><label>Choose gender</label>
    <select class="select-gender" id="gender" name="gender">
      <option value="M">Male</option>
      <option value="F">Female</option>
    </select></div>
    <div class="birthdate-form">
    <label>Choose birthdate</label>
    <input type="text" class="birthday-input" id="birthday" name="birthday" placeholder="Choose date"></div>
    </div>
  </form>`;  
  return new Promise((resolve, reject) => {
      const modal = MyWind.modal({
        title: options2.title || "Add student",
        width: '400px',
        closable: false,
        content: html_form,
        onClose() {
          modal.destroy()
        },
        footerButtons: [
            {text: 'Cancel', type: 'cancel-type', handler() {
              modal.close()
              reject()
            }},
            {text: options2.text || 'Create', type: 'create-type', handler() {
              if($('#form-student').valid()){
                modal.close()
                const info = getInformation()
                if(!info){
                    reject()
                }
                if(options.id){
                    info.id = options.id
                }
                resolve(info)
              }
              
            }}
        ]
      })
      setTimeout(() => modal.open(), 100)

      $(document).ready(function(){
        setInformation(options)

        $.validator.addMethod("customUppercase", function(value, element) {
          return /^[A-Z]/.test(value)
        }, "First letter must be uppercase.")

        $.validator.addMethod("birthYearGreaterThan2007", function(value, element) {
          let birthYear = new Date(value);
          return birthYear.getFullYear() <= 2007;
        }, "Birth year must be at least 2007.")

        $.validator.addMethod("birthYearLowerThan1960", function(value, element) {
          let birthYear = new Date(value);
          return birthYear.getFullYear() >= 1960;
        }, "Birth year must be lower than 1960.")
        
        $('#form-student').validate({
          rules: {
            group: {
              required: true
            },
            firstName: {
              required: true,
              customUppercase: true
            },
            lastName: {
              required: true,
              customUppercase: true
            },
            gender: {
              required : true
            },
            birthday: {
              required: true,
              date: true,
              birthYearGreaterThan2007: true,
              birthYearLowerThan1960: true
            }
          }
        })
      })
  }) 
    
}
