MyWind.confirmForm = function(options){
  return new Promise((resolve, reject) => {
      const modal = MyWind.modal({
        title: options.title,
        width: '400px',
        closable: false,
        content: options.content,
        onClose() {
          modal.destroy()
        },
        footerButtons: [
            {text: 'Cancel', type: 'cancel-type', handler() {
              modal.close()
              reject()
            }},
            {text: 'Delete', type: 'delete-type', handler() {
              modal.close()
              resolve()
            }}
        ]
      })
      setTimeout(() => modal.open(), 100)
  }) 
}
