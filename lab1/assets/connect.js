const socket = io();

socket.on('connect', () => {
    console.log('Підключено до сервера WebSocket');
    // socket.userId = userId;
});
 
socket.on('disconnect', () => {
    console.log('Відключено від сервера WebSocket');
});
const messages = document.querySelector('.message')


socket.on('message', (data) => {// отримуєш тута 

    console.log('Received message from server:', data, data.owner, data.text);
    let chatId = document.querySelector('.btn-submit').dataset.id;
    if(chatId === data.chatId){
        addMessage(data, Name)
    } else{
        add_to_unread(data)
        get_users_unread()
    }
    
    //перевіряти чи чат відкритий , якщо ніто додавати доднепрочитагних
    
});
function add_to_unread(data) {
    $.ajax({
        type: "POST", 
        url: '/add_to_unread',
        data: {chatId: data.chatId, userId: data.getterId}, 
        success: function(response) { 
            console.log("success")
          console.log(response)
        },
        error: function(xhr, status, error) { 
          console.error('Error:', error); 
        }
      });
}

function get_users_unread() {
    $.ajax({
        type: "POST", 
        url: '/get_users_unread',
        data: {}, 
        success: function(response) { 
            try{
                let info = JSON.parse(response)
                console.log(info, 'sdfgfdsa')
                if(info.length == 0){
                    document.querySelector('.notifications').style.display = 'none';
                } else{
                    document.querySelector('.notifications').style.display = 'flex';
                }
                // "user":"663e5efcf2ea258e8addf366","chat":"hehe","chatId":"6645efec35aaf72a08b5e171","number":5,"sender":"sofiia","latestMessage":"cxxxc"},{"user":"663e5efcf2ea258e8addf366","chat":"hu","chatId":"6645efc38e46964f1be61d0e","number":1,"sender":"sofiia","latestMessage":"hu"}]
                document.querySelector('.notifications').innerHTML = ``
                info.forEach(msg =>{
                    let node = document.createElement('button');
                    node.classList.add('btn', 'unread-msg');
                    node.id = msg.chatId;
                    node.style.zIndex = 1000;

                    node.onclick = function() {
                    localStorage.setItem('clickedButtonId', msg.chatId);
                    window.location.href = '/chats';
                    };

                    let notifBox = document.querySelector('.notifications');
                    notifBox.style.zIndex = 1000;

                    node.innerHTML += `
                    <div class="message-box d-flex align-items-center">
                        <img src="assets/avatars/avatar-${msg.senderAvatar}.svg" alt="User picture" class="friend-pic rounded-circle">  <div class="user-msg flex-grow-1 ml-2">  <label class="friend-username">${msg.chat}</label>
                        <p class="message font-weight-bold">${msg.sender}: ${msg.latestMessage}</p>  </div>
                    </div>
                    `;

                    notifBox.appendChild(node);
                })
                

                console.log(response)
            }catch(err){
                let msg_box =  document.querySelector(".notifications");
                msg_box.innerHTML = "";
                console.log(err)
            }
           
        },
        error: function(xhr, status, error) { 
          console.error('Error:', error); 
        }
      });
  }

get_users_unread()


function createChat(usernameChat) {
    socket.emit('createChat', { name: usernameChat });
}
socket.on('new_task', (data) => {
    console.log("add msg")
    if(document.querySelector(".tasks")){
        console.log('Нова таска', data);
        let node = document.createElement("div");
        node.id  = data.id;
        console.log(formatDate(data.deadline))
        data.deadline = formatDate(data.deadline);
        node.innerHTML = task_html(data);
        node.classList.add('taskbox');
        node.addEventListener('click',(event)=>{
            move_to_next_cathegory(node.id)
        });
        document.querySelector(".todo").appendChild(node);
    }else{
        
        if(Name!='Unauthorized'){
        data.deadline = formatDate(data.deadline);
        showNotification(`${data.name}(${data.deadline})`)}
    }
   
});

socket.on('createChat', (chat) => {
    console.log('Received created chat:', chat);
    
    const chatsListElement = document.querySelector('.chat-list');
  
    const chatBtn = document.createElement('button')
    chatBtn.classList.add('chat-list-item', 'btn', 'btn-outline-secondary', 'btn-block', 'mb-2')
    chatBtn.id = chat.chatId
    chatBtn.addEventListener('click', (event)=>{
      openChat(event.target.id, event.target.innerHTML)

      document.querySelectorAll(".chat-list-item").forEach(elem=>{
        elem.classList.remove("active-chat");
      })
      event.target.classList.add("active-chat");
    })
    chatBtn.innerHTML = chat.title;
    chatsListElement.appendChild(chatBtn);
});

document.querySelector('.add-chat-btn').addEventListener('click', () => {
    MyWind.addChatForm({title: 'Add Chat', text: 'Add'}).then((usernameChat) => {
        console.log('Username entered:', usernameChat);
        // Call your createChat function with the entered username
        createChat(usernameChat);
      })
      .catch(() => {
        console.log('User cancelled');
      });
});

const form = document.getElementById('chat-form');
form.addEventListener('submit', (event) => {//можна без форми, при відкритті чату до кнопки надсилання додавати айді чату,
    // надсилати на сервер треба не тільки повідомлення , але й айді чату
    event.preventDefault();
    const input = document.getElementById('message');
    const text = input.value;
    if (text) {
        let chatId = document.querySelector('.btn-submit').dataset.id;
        
        console.log('Надіслано повідомлення від сервера WebSocket:', text);
        let res = {data: {text: text, chatId: chatId}}
        socket.emit('message',res);
        input.value = ''; 
    }
});

function addMessage(message, username) {
    const messages = document.querySelector('.chat-messages');
    const messageBlock = document.createElement('div');
    messageBlock.classList.add('message-block', 'pb-4');
    messageBlock.classList.add(message.owner === username ? "chat-message-right" : "chat-message-left");

    messageBlock.innerHTML = `
      <img src='assets/avatars/avatar-${message.owner_av}.svg' class="rounded-circle mr-3" alt='${message.owner}' width='40' height='40'>
      <div class="flex-shrink-1 bg-light rounded py-2 px-3 mr-3">
        <div class="font-weight-bold mb-1">${message.owner === username ? 'You' : message.owner}</div>
        ${message.text}
      </div>
      `;

    messages.appendChild(messageBlock);
    let chatInner = document.querySelector('.chat-messages');
    chatInner.scrollTop = chatInner.scrollHeight;
}

document.querySelector('.change-title-btn').addEventListener('click', ()=>{
    MyWind.addChatForm({title: 'Change title', text: 'Change'}).then((newTitle) => {
        console.log('Title entered:', newTitle);
        let chatId = document.querySelector('.btn-submit').dataset.id;
        let res = {title: newTitle, chatId}
        socket.emit('renameChat', res)
      })
      .catch(() => {
        console.log('User cancelled');
      });
})

socket.on('renameChat', (data) => {
    console.log(data.title)
    document.querySelector('.chat-title').textContent = data.title;  
    const elements = document.querySelectorAll('.chat-list-item');
    console.log(elements)
    elements.forEach(element => {
        console.log(element)
        if (element.id === data.chatId) {
            element.innerHTML = data.title;
        }
    });
   
});

document.querySelector('.add-user').addEventListener('click', ()=>{
    MyWind.addChatForm({title: 'Add user', text: 'Add'}).then((user) => {
        console.log('Title entered:', user);
        let chatId = document.querySelector('.btn-submit').dataset.id;
        let res = {name: user, chatId}
        socket.emit('addUserToChat', res)
      })
      .catch(() => {
        console.log('User cancelled');
      });
})

socket.on('addUserToChat', (data) => {
    console.log(data.name)
    let chatId = document.querySelector('.btn-submit').dataset.id;
    if(chatId === data.chatId){
        document.querySelector('.users-in-chat').textContent += data.name;  
    }

});


function formatDate(dateString) {
    let dateObject = new Date(dateString);
    let year = dateObject.getFullYear();
    let month = (dateObject.getMonth() + 1).toString().padStart(2, '0'); // Додаємо 1, оскільки місяці в JavaScript починаються з 0
    let day = dateObject.getDate().toString().padStart(2, '0');

    return `${year}-${month}-${day}`;
}

function move_to_next_cathegory(id){
    console.log(id)
    socket.emit('move_to_next_cathegory',{id:id} );
}  

socket.on('move_to_next_cathegory', (data) => {
    if(document.querySelector(".tasks")){
        console.log('Нова таска', data);
        let node = document.getElementById(data.id);
        switch (data.status) {
            case 'ToDo':
                document.querySelector(".done").removeChild(node);
                document.querySelector(".todo").appendChild(node);
                break;
            case 'WIP':
                document.querySelector(".todo").removeChild(node);
                document.querySelector(".wip").appendChild(node);
                break;
            case 'Done':
                document.querySelector(".wip").removeChild(node);
                document.querySelector(".done").appendChild(node);
                break;
            default:
                console.log('Невідомий статус');
                break;
        }
    }
});



/////////////////////////////////////////////////

