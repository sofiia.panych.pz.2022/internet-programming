document.querySelector('.form').addEventListener('submit', (event) => {
  event.preventDefault();
  
  if (document.querySelector('.check').checked) {
    console.log('helo');
    $.validator.addMethod("birthValid", function(value, element) {
      let birthYear = new Date(value);
      return birthYear.getFullYear() <= 2007;
    }, "Birth year must be at least 2007.");

    $.validator.addMethod("passwordValid", function(value, element) {
      return /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,}$/.test(value);
    }, "Password is not valid");

    $('form').validate({
      rules: {
        email: {
          required: true,
          email: true
        },
        password: {
          required: true,
          passwordValid: true
        },
        password2: {
          required: true,
          equalTo: '.password'
        },
        date: {
          required: true,
          date: true,
          birthValid: true
        }
      },
    });
  } else {
    console.log('bye');
  }
});
